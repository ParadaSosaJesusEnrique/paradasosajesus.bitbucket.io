var dom=(function(){
    var _crear_elemento=function(str_etiqueta,obj_propiedades,obj_atributos){
        var nuevo_elemento=document.createElement(str_etiqueta);
        for(var llave in obj_propiedades){
            nuevo_elemento[llave]=obj_propiedades[llave];
        }
        for(var clave in obj_atributos){
            nuevo_elemento.setAttribute(clave,obj_atributos[clave]);            
       }
        return nuevo_elemento;
    };


    return  {
        "crear_elemento": _crear_elemento
    }

}());
